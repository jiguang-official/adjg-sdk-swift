//
//  ADJgSplashViewController.swift
//  ADJgSDKDemo-iOS-Swift
//
//  Created by 陈坤 on 2020/6/16.
//  Copyright © 2020 陈坤. All rights reserved.
//

import UIKit

class ADJgSplashViewController: UIViewController, ADJgSDKSplashAdDelegate {

    
    var splash: ADJgSDKSplashAd?
    
    let normalView = ADJgSkipView.init()
    let ringView = ADJgSkipRingView.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.init(red: 225/255.0, green: 233/255.0, blue: 239/255.0, alpha: 1)
        
        // 加载开屏
        loadSplash()
    }
    
    func loadSplash() {
        // 1、初始化开屏广告对象
        splash = ADJgSDKSplashAd.init()
        // 设置代理
        splash?.delegate = self
        // 2、设置广告位id
        splash?.posId = "88d136b3d8c8da294e"
        // 设置广告请求超时时间
        splash?.tolerateTimeout = 4
        
        // 3、设置广告背景颜色，推荐设置为启动图的平铺颜色
        let bgImage = UIImage.init(named: "750x1334.png")
        splash?.backgroundColor = UIColor.adjg_get(with: bgImage!, withNewSize: UIScreen.main.bounds.size)
    
        // 设置控制器，用于落地页弹出
        splash?.controller = self
        
        // 4、初始化底部视图
        var bottomViewHeight:CGFloat = SCREEN_HEIGHT * 0.15
        
        let bottomView = UIView.init(frame: CGRect.init(x: 0, y: SCREEN_HEIGHT - bottomViewHeight, width: SCREEN_WIDTH, height: bottomViewHeight))
        bottomView.backgroundColor = UIColor.white
        
        let logoImageView = UIImageView.init(image: UIImage.init(named: "ADJg_Logo.png"))
        logoImageView.frame = CGRect.init(x: (SCREEN_WIDTH - 135)/2, y: (bottomViewHeight - 46)/2, width: 135, height: 46)
        
        bottomView.addSubview(logoImageView)
        // 5、加载全屏的开屏广告
        // splash?.loadAndShow(in: self.window! , withBottomView: nil)
        // 5、加载带有底部视图的开屏广告
        splash?.loadAndShow(in: UIApplication.shared.keyWindow!, withBottomView: bottomView)
        
    }
    
    func adjg_splashAdSuccess(toLoad splashAd: ADJgSDKSplashAd) {
        print(#function)
        let extInfo = splashAd.adjg_extInfo()
        print("ecpm=", extInfo?.ecpm ?? "")
        print("ecpmType=", extInfo?.ecpmType.rawValue ?? 0)
    }
    
    func adjg_splashAdSkip(_ splashAd: ADJgSDKSplashAd) {
        print(#function)
    }

    func adjg_splashAdClosed(_ splashAd: ADJgSDKSplashAd) {
        print(#function)
        splash = nil
    }
    
    func adjg_splashAdFail(toPresentScreen splashAd: ADJgSDKSplashAd, failToPresentScreen error: ADJgAdapterErrorDefine) {
        print(#function)
        splash = nil
    }

}
