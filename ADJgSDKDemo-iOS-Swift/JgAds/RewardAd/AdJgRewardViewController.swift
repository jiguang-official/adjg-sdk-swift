//
//  ADJgRewardViewController.swift
//  ADJgSDKDemo-iOS-Swift
//
//  Created by 陈坤 on 2020/6/16.
//  Copyright © 2020 陈坤. All rights reserved.
//

import UIKit

class AdJgRewardViewController: UIViewController, ADJgSDKRewardvodAdDelegate {
    func adjg_rewardvodAdServerDidSucceed(_ rewardvodAd: ADJgSDKRewardvodAd, info: [AnyHashable : Any]) {
        
    }
    
    func adjg_rewardvodAdCloseLandingPage(_ rewardvodAd: ADJgSDKRewardvodAd) {
        
    }
    
    func adjg_rewardvodAdServerDidSucceed(_ rewardvodAd: ADJgSDKRewardvodAd) {
        
    }
    
    func adjg_rewardvodAdServerDidFailed(_ rewardvodAd: ADJgSDKRewardvodAd, errorModel: ADJgAdapterErrorDefine) {
        
    }
    
    func adjg_rewardvodAdLoadSuccess(_ rewardvodAd: ADJgSDKRewardvodAd) {
        let extInfo = rewardvodAd.adjg_extInfo()
        print("ecpm=", extInfo?.ecpm ?? "")
        print("ecpmType=", extInfo?.ecpmType.rawValue ?? 0)
    }
    
    func adjg_rewardvodAdReady(toPlay rewardvodAd: ADJgSDKRewardvodAd) {
        // 3、展示激励视频广告
        if self.rewardAd!.rewardvodAdIsReady(){
            isReady = true
            // 建议在这个时机进行展示 也可根据需求在合适的时机进行展示
            // [self.rewardvodAd showRewardvodAd];
        }
        self.view.makeToast("激励视频准备完成")
    }
    
    func adjg_rewardvodAdVideoLoadSuccess(_ rewardvodAd: ADJgSDKRewardvodAd) {
        
    }
    
    func adjg_rewardvodAdWillVisible(_ rewardvodAd: ADJgSDKRewardvodAd) {
        
    }
    
    func adjg_rewardvodAdDidVisible(_ rewardvodAd: ADJgSDKRewardvodAd) {
        
    }
    
    func adjg_rewardvodAdDidClose(_ rewardvodAd: ADJgSDKRewardvodAd) {
        rewardAd = nil
    }
    
    func adjg_rewardvodAdDidClick(_ rewardvodAd: ADJgSDKRewardvodAd) {
        
    }
    
    func adjg_rewardvodAdDidPlayFinish(_ rewardvodAd: ADJgSDKRewardvodAd) {
        
    }
    
    func adjg_rewardvodAdDidRewardEffective(_ rewardvodAd: ADJgSDKRewardvodAd) {
        
    }
    
    func adjg_rewardvodAdFail(toLoad rewardvodAd: ADJgSDKRewardvodAd, errorModel: ADJgAdapterErrorDefine) {
        rewardAd = nil
    }
    
    func adjg_rewardvodAdPlaying(_ rewardvodAd: ADJgSDKRewardvodAd, errorModel: ADJgAdapterErrorDefine) {
        
    }
    

    var rewardAd : ADJgSDKRewardvodAd?
    var isReady : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.init(red: 225/255.0, green: 233/255.0, blue: 239/255.0, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        let loadBtn = UIButton.init()
        loadBtn.layer.cornerRadius = 10;
        loadBtn.clipsToBounds = true;
        loadBtn.backgroundColor = UIColor.white
        loadBtn.setTitle("加载激励视频", for: .normal)
        loadBtn.setTitleColor(UIColor.black, for: .normal)
        self.view.addSubview(loadBtn)
        loadBtn.frame = CGRect.init(x: 30, y: UIScreen.main.bounds.size.height/2-60, width: UIScreen.main.bounds.size.width-60, height: 55)
        loadBtn.addTarget(self, action: #selector(loadRewardAd), for: .touchUpInside)
        
        let showBtn = UIButton.init()
        showBtn.layer.cornerRadius = 10
        showBtn.clipsToBounds = true
        showBtn.backgroundColor = UIColor.white
        showBtn.setTitle("展示激励视频", for: .normal)
        showBtn.setTitleColor(UIColor.black, for: .normal)
        self.view.addSubview(showBtn)
        showBtn.frame = CGRect.init(x: 30, y: UIScreen.main.bounds.size.height/2+20, width: UIScreen.main.bounds.size.width-60, height: 55)
        showBtn.addTarget(self, action: #selector(showRewardAd), for: .touchUpInside)
        
        
    }
    
    
    @objc func loadRewardAd() {
        // 1、初始化激励视频广告对象
        self.rewardAd = ADJgSDKRewardvodAd.init()
        self.rewardAd?.controller = self
        self.rewardAd?.delegate = self
        self.rewardAd?.tolerateTimeout = 5
        self.rewardAd?.posId = "a2b2644e75983ae44d"
        // 2、加载激励视频广告
        self.rewardAd?.load()
    }
    
    @objc func showRewardAd() {
        if isReady && self.rewardAd!.rewardvodAdIsReady() {
            self.rewardAd?.show()
        } else {
            self.view.makeToast("激励视频未准备好")
        }
    }

}
