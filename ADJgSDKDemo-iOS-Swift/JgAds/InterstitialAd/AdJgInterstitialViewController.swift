//
//  ADJgInterstitialViewController.swift
//  ADJgSDKDemo-iOS-Swift
//
//  Created by 陈坤 on 2020/6/17.
//  Copyright © 2020 陈坤. All rights reserved.
//

import UIKit

class AdJgInterstitialViewController: UIViewController, ADJgSDKIntertitialAdDelegate {
    func adjg_interstitialAdCloseLandingPage(_ interstitialAd: ADJgSDKIntertitialAd) {
        
    }
    
    func adjg_interstitialAdSucced(toLoad interstitialAd: ADJgSDKIntertitialAd) {
        print(#function)
        let extInfo = interstitialAd.adjg_extInfo()
        print("ecpm=", extInfo?.ecpm ?? "")
        print("ecpmType=", extInfo?.ecpmType.rawValue ?? 0)
        // 3、展示插屏广告
        isReady = true
        self.view.makeToast("插屏广告准备完成")
    }
    
    func adjg_interstitialAdFailed(toLoad interstitialAd: ADJgSDKIntertitialAd, error: ADJgAdapterErrorDefine) {
        print(#function)
        self.interstitialAd = nil
    }
    
    func adjg_interstitialAdDidPresent(_ interstitialAd: ADJgSDKIntertitialAd) {
        print(#function)
    }
    
    func adjg_interstitialAdFailed(toPresent interstitialAd: ADJgSDKIntertitialAd, error: Error) {
        print(#function)
    }
    
    func adjg_interstitialAdDidClick(_ interstitialAd: ADJgSDKIntertitialAd) {
        print(#function)
    }
    
    func adjg_interstitialAdDidClose(_ interstitialAd: ADJgSDKIntertitialAd) {
        print(#function)
        self.interstitialAd = nil
    }
    
    func adjg_interstitialAdExposure(_ interstitialAd: ADJgSDKIntertitialAd) {
        print(#function)
    }
    
    var interstitialAd : ADJgSDKIntertitialAd?
    var isReady:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.init(red: 225/255.0, green: 233/255.0, blue: 239/255.0, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        let loadBtn = UIButton.init()
        loadBtn.layer.cornerRadius = 10;
        loadBtn.clipsToBounds = true;
        loadBtn.backgroundColor = UIColor.white
        loadBtn.setTitle("加载插屏广告", for: .normal)
        loadBtn.setTitleColor(UIColor.black, for: .normal)
        self.view.addSubview(loadBtn)
        loadBtn.frame = CGRect.init(x: 30, y: UIScreen.main.bounds.size.height/2-60, width: UIScreen.main.bounds.size.width-60, height: 55)
        loadBtn.addTarget(self, action: #selector(loadInterstitialAd), for: .touchUpInside)
        
        let showBtn = UIButton.init()
        showBtn.layer.cornerRadius = 10
        showBtn.clipsToBounds = true
        showBtn.backgroundColor = UIColor.white
        showBtn.setTitle("展示插屏广告", for: .normal)
        showBtn.setTitleColor(UIColor.black, for: .normal)
        self.view.addSubview(showBtn)
        showBtn.frame = CGRect.init(x: 30, y: UIScreen.main.bounds.size.height/2+20, width: UIScreen.main.bounds.size.width-60, height: 55)
        showBtn.addTarget(self, action: #selector(showInterstitialAd), for: .touchUpInside)
    }
    
    @objc func loadInterstitialAd() {
        // 1、初始化插屏广告对象
        self.interstitialAd = ADJgSDKIntertitialAd.init()
        self.interstitialAd?.delegate = self
        self.interstitialAd?.controller = self
        self.interstitialAd?.tolerateTimeout = 4
        self.interstitialAd?.posId = "75dc0e44ed48bc2a62"
        if SetConfigManager.shared().fullAdAdScenceId != "" {
            self.interstitialAd?.scenesId = SetConfigManager.shared().fullAdAdScenceId
        }
        // 2、加载插屏广告
        self.interstitialAd?.loadData()
    }
    
    @objc func showInterstitialAd() {
        if isReady  {
            self.interstitialAd?.show()
        }else {
            self.view.makeToast("插屏广告未准备完成")
        }
    }
    

}
